<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>        महर्षि दयानन्द इण्टर  कॉलेज
</title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

    <!-- bootstrap4 css link -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- bootstrap4 js and jquery links -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

</head>

<body>
    <?php include('header.php') ?>
    
    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div style="box-shadow: 0px 0px 20px rgb(134, 134, 134);">
                    <div class="p-3">
                        <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block img-fluid w-100" style="max-height: 400px;" src="image/img21.jpg" alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100 img-fluid" style="max-height: 400px;" src="image/img2.jpg" alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100 img-fluid" style="max-height: 400px;" src="image/img20.jpg" alt="Third slide">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="border mt-4 "> -->
    <div class="container  mt-4">
        <div class="card">
            <div class="card-body">
            </div>
            <section>
        <div class="container">
            <div class="section-title text-center mb-4">
                <h3>संपर्क करें</h3>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="container-fluid">
                        <div class="map-responsive">
                            <div class="">
                                <iframe
                                    src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0s1a7phLN0iaD6-UE7m4qP-z21pH0eSc&q=Eiffel+Tower+Paris+France"
                                    width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <form class="p-4 text-center" style="box-shadow: 0px 0px 5px rgb(194, 194, 194);">
                        <div class="row">
                            <div class="col mb-2">
                                <label for="exampleInputname">आपका नाम</label>
                                <input type="text" class="form-control" id="exampleInputname"
                                    aria-describedby="emailHelp" placeholder="आपका नाम">
                            </div>
                            <div class="col mb-2">
                                <label for="exampleInputEmail1">ईमेल</label>
                                <input type="email" class="form-control" id="exampleInputEmail1"
                                    aria-describedby="emailHelp" placeholder="ईमेल">
                            </div>
                        </div>
                        <div class="form-group mb-2">
                            <label for="exampleInputsubject">विषय</label>
                            <input type="text" class="form-control" id="exampleInputsubject" placeholder="विषय">
                        </div>
                        <div class="form-group mb-2">
                            <label for="exampleFormControlTextarea1">संदेश</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn submitbtn mt-3">प्रस्तुत</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

                    </div>

                    </div>

                </div>

        </div>

    </div>



    <!-- </div> -->



    <?php include('footer.php')?>


</body>

</html>