<div style="background-color: rgb(3, 128, 134);" class="mt-4">
    <div class="container py-4 footertop">
        <div class="row">
            <div class="col-md-3">
                <h5 class="lh-lg fw-bold mb-2 text-light font-sans-serif">पता</h5>
                <ul class="list-unstyled mb-md-4 mb-lg-0">
                    <li class="lh-lg" ><a class="text-200" href="#!"><i class="fas fa-map-marker-alt"></i>
                    ढाकी - हुसैनपुर  बस स्टॉप , नजीबाबाद -बिजनौर रोड.
                            </a></li>
                    <!-- <li class="lh-lg"><a class="text-200" href="#!"> <i class="fas fa-envelope"></i> -->
                            <!-- infozoyo@gmai</a></li> -->
                    <li class="lh-lg"><a class="text-200" href="#!"> <i class="fas fa-phone"></i>
                            9012122707</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5 class="lh-lg fw-bold mb-2 text-light font-sans-serif">महत्वपूर्ण कड़ी</h5>
                <ul class="list-unstyled mb-md-4 mb-lg-0">
                    <li class="lh-lg"><a class="text-200" href="index.php">होम </a></li>
                    <li class="lh-lg"><a class="text-200" href=""> बारे में </a></li>
                    <li class="lh-lg"><a class="text-200" href="contact.php">संपर्क करें</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5 class="lh-lg fw-bold text-light mb-2 font-sans-serif">अन्य लिंक</h5>
                <ul class="list-unstyled mb-md-4 mb-lg-0">
                    <!-- <li class="lh-lg"><a class="text-200" href="#!"> Career</a></li> -->
                    <li class="lh-lg"><a class="text-200" href="#!">समाचार</a></li>
                    <li class="lh-lg"><a class="text-200" href="#!">आयोजन</a></li>
                </ul>
            </div>
            <div class="col-md-3 footertoplinks">
                <h5 class="lh-lg fw-bold text-light font-sans-serif">हमारा अनुसरण करें</h5>
                <div class="mt-2">
                    <a href="#"><i class="fab fa-facebook mx-1"></i></a>
                    <a href="#"><i class="fab fa-instagram mx-1"></i></a>
                    <a href="#"><i class="fab fa-twitter mx-1"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="background-color: rgb(3, 115, 121); color: white;">
    <div class="container-fluid py-4 text-center">
        <div class="row">
            <div class="col-md-4">
                <span>&copy;Copyright 2021 School, All Rights Reserved.</span>
            </div>
            <div class="col-md-4">
                <span>Privacy&middot;Policy</span>
            </div>
            <div class="col-md-4">
                Developed by: <strong>Zoyo E-commerce Pvt. Ltd.</strong>
            </div>
        </div>
    </div>
</div>