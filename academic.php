<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>        महर्षि दयानन्द इण्टर  कॉलेज
</title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

    <!-- bootstrap4 css link -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- bootstrap4 js and jquery links -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

</head>

<body>
    <?php include('header.php') ?>

    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div style="box-shadow: 0px 0px 20px rgb(134, 134, 134);">
                    <div class="p-3">
                        <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block img-fluid w-100" style="max-height: 400px;"
                                        src="image/img21.jpg" alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100 img-fluid" style="max-height: 400px;" src="image/img2.jpg"
                                        alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100 img-fluid" style="max-height: 400px;"
                                        src="image/img20.jpg" alt="Third slide">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls1" role="button"
                                data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls1" role="button"
                                data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="border mt-4 "> -->
    <div class="container  mt-4">
        <div class="card">
            <div class="card-body">
                <h3 class="ml-4 text-center" style="font-weight: 600;">शैक्षणिक सुविधा</h3>
                <div class="row mt-4">
                    <div class="col-md-6">
                        <h5>भाषा प्रयोगशाला :
                        </h5>
                        <p class="mt-4">
                            भाषा प्रयोगशाला एक नेटवर्क अनुप्रयोग है जो आधुनिक भाषा शिक्षण में एक सहायता के रूप में
                            प्रयोग किया जाता है। ... यह व्यक्तिगत ध्यान का लाभ उठाने सुनने और विधि ओएस सुनने के माध्यम
                            से सीखने और एक पूरी तरह से कम्प्यूटरीकृत वातावरण में सीखने की पद्धति के माध्यम से भाषा में
                            महारत हासिल करने के लिए एक आसान तरीका प्रदान करता है।

                        </p>
                    </div>
                    <div class="col-md-6">
                        <img src="image/img22.jpg" class="img-fluid" alt="">
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <img src="image/img23.jpg" class="img-fluid" alt="">
                    </div>
                    <div class="col-md-6">
                        <h5 class="mt-5">विज्ञान प्रयोगशाला :

                        </h5>
                        <p class="mt-4">
                            विद्यालयों में विज्ञान प्रयोगशाला वह स्थान है जहाँ उपयुक्त अभिव्यक्ति एवं निर्धारित प्रयोगों
                            के समुच्चय को सुव्यवस्थित ढंग से सम्पन्न करके मूल प्रयोगिक कौशलों को सीखा जाता है। ...
                            विद्यार्थी को प्रयोगशाला में उपलब्ध सामान्य सुविधाओं तथा उपकरणों के विषय में आवश्यक जानकारी
                            और नियम तथा हिदायतों का पालन करना आवश्यक है।
                        </p>
                    </div>

                </div>
                <div class="row mt-4">
                    <div class="col-md-6">
                        <h5>कंप्यूटर लैब:
                        </h5>
                        <p class="mt-4">
                            कंप्यूटर लैब पूरी कक्षा को कंप्यूटर उपयोग सिखाने के केंद्र के रूप में कार्य करता है। कक्षा
                            के शिक्षक भी अनुसंधान के लिए, या प्रौद्योगिकी-आधारित परियोजनाओं को बनाने के लिए अपनी कक्षाओं
                            के साथ प्रयोगशाला का उपयोग करते हैं। शिक्षण और सीखने की प्रक्रिया में सूचना प्रौद्योगिकी को
                            एकीकृत करना विषय-केंद्रित और सॉफ्टवेयर-केंद्रित दृष्टिकोणों के माध्यम से किया जा सकता है।
                            सूचना प्रौद्योगिकी के विस्फोट के साथ-साथ गणितीय विज्ञान में शिक्षण और सीखने को कंप्यूटर लैब
                            में उपलब्ध गणितीय और सांख्यिकीय सॉफ्टवेयर पैकेजों की विविधता के साथ आगे बढ़ाया गया है।
                        </p>
                    </div>
                    <div class="col-md-6">
                        <img src="image/img10.jpg" class="img-fluid"  alt="">
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-6">
                            <img src="image/gettyimages-1193273154-170667a.jpg" alt="">
                        </div>
                        <div class="col-md-6">
                            <h5 class="mt-4">
                                Library and Digital Library:
                            </h5>
                            <p class="mt-4">
                                A library is a space where we provide collection of diverse learning resources to
                                support a school’s curriculum, meet individual students’ needs and interests, and ensure
                                that young people develop information literacy skills within the school’s curriculum.
                                This concept of a learning resource center is both a social development of the twentieth
                                century and an evolution of information exchange.
                            </p>
                        </div>
                    </div> -->

                </div>

            </div>

        </div>

    </div>



    <!-- </div> -->



    <?php include('footer.php')?>


</body>

</html>