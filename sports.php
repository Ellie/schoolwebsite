<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>        महर्षि दयानन्द इण्टर  कॉलेज
</title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

    <!-- bootstrap4 css link -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- bootstrap4 js and jquery links -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

</head>

<body>
    <?php include('header.php') ?>

    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div style="box-shadow: 0px 0px 20px rgb(134, 134, 134);">
                    <div class="p-3">
                        <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block img-fluid w-100" style="max-height: 400px;"
                                        src="image/img21.jpg" alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100 img-fluid" style="max-height: 400px;" src="image/img2.jpg"
                                        alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100 img-fluid" style="max-height: 400px;"
                                        src="image/img20.jpg" alt="Third slide">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls1" role="button"
                                data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls1" role="button"
                                data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="border mt-4 "> -->
    <div class="container  mt-4">
        <div class="card p-2">
            <div class="card-body">
                <h3 class="ml-4 text-center" style="font-weight: 600;">खेल सुविधा</h3>
                <div class="row mt-4">
                    <div class="col-md-6">
                        <h5>बैडमिंटन:
                        </h5>
                        <p class="mt-4">
                            बैडमिंटन रैकेट से खेला जानेवाला एक खेल है, जो दो विरोधी खिलाडियों (एकल) या दो विरोधी जोड़ों
                            (युगल) द्वारा नेट से विभाजित एक आयताकार कोर्ट में आमने-सामने खेला जाता है खिलाड़ी अपने रैकेट
                            से शटलकॉक को मारकर के अपने विरोधी पक्ष के कोर्ट के आधे हिस्से में गिराकर प्वाइंट्स प्राप्त
                            करते हैं।
                        </p>
                    </div>
                    <div class="col-md-6">
                        <img src="image/img26.jpg" alt="">
                    </div>

                </div>

                <div class="row mt-4">
                    <div class="col-md-6">

                        <img src="image/foottttt.jpg" alt="">
                    </div>
                    <div class="col-md-6">
                        <h5 class="mt-4">फ़ुटबॉल:
                        </h5>

                        <p class="mt-4">
                            फुटबॉल मैच 90 मिनट के लिए खेला जाता है जिसमें दो हिस्सों और 15 मिनट का ब्रेक होता है।
                            प्रत्येक टीम में 11 खिलाड़ी होते हैं और खिलाड़ियों को अपने पैर से गेंद को मारना होता है। एक
                            गोल पाने के लिए गेंद के साथ विरोधी टीम के गोलपोस्ट को छूना होता है
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="mt-4">
                                क्रिकेट :
                            </h5>
                            <p class="mt-4">
                                भारत में क्रिकेट का खेल कई वर्षों से खेला जा रहा है, यह एक काफी प्रसिद्ध तथा रोमांचक खेल
                                है। इसे खेल को बच्चों द्वारा काफी पसंद किया जाता है सामान्यतः छोटे मैदान, सड़क जैसे आदि
                                जैसे किसी भी छोटे खुले स्थानों पर उनकी क्रिकेट खेलने की आदत होती है। बच्चे क्रिकेट और
                                उसके नियम-कानूनों के बारे में जानकारी के शौकीन होते है। भारत में राष्ट्रीय और
                                अंतरराष्ट्रीय स्तर पर खेले जाने वाले में खेलों में क्रिकेट सबसे अधिक प्रसिद्ध है। लोगों
                                में क्रिकेट की लोकप्रियता इतनी अधिक है कि इस खेल को देखने के लिए दर्शकों की जितनी भीड़
                                स्टेडियम में जाती है उतनी शायद ही किसी दूसरे खेल में जाती हो।
                            </p>
                            
                        </div>
                        <div class="col-md-6">

                            <img src="image/illustration-featuring-two-little-boys-260nw-207188077.jpg" alt="">
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>



    <!-- </div> -->



    <?php include('footer.php')?>


</body>

</html>