<div class="nav">
    <div class="container-fluid">
        <div class="row navlinks mt-2 justify-content-between">
            <div class="col-md-4 d-none d-lg-block topnavlinks text-center">
                <a href="" style="color: white;"><span><i class="fas fa-map-marker-alt mr-1"></i>ढाकी - हुसैनपुर </span></a>
                <a href="" style="color: white;"><span><i class="fas fa-envelope ml-2 mr-1"></i>mdic1218@gmail.com</span></a>
            </div>
            <div class="col-md-3 text-center topnavicon">
                <a href=""><span><i class="fab fa-facebook mx-2"></i></span></a>
                <a href=""><span><i class="fab fa-instagram mx-2"></i></span></a>
                <a href=""><span><i class="fab fa-twitter mx-2"></i></span></a>
                <a href="" class="ml-4"><span><i class="fas fa-sign-in-alt mr-1"></i>Login</span></a>
            </div>

        </div>
    </div>
</div>

<nav class="navbar navbar-expand-lg sticky-top py-4 ">
    <a class="collegename" href="index.php">
        <h4>
        महर्षि दयानन्द इण्टर  कॉलेज
        </h4>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class=""><i class="fas fa-bars" style="color: white;"></i></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">होम <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                बारे में 
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="rough.pdf">बारे में </a>
                    <!-- <a class="dropdown-item" href="principal.php">Directer's Message</a> -->
                    <!-- <a class="dropdown-item" href="mission vision.php">Mission & vision</a> -->
                    <!-- <a class="dropdown-item" href="ourmotto.php">Motto</a> -->

                </div>
            </li>
            <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Admission
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="admissionprocess.php">Admission Process</a>
                    <a class="dropdown-item" href="#"> </a>
                </div>
            </li> -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                सुविधाएं
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="academic.php">शैक्षणिक सुविधा</a>
                    <a class="dropdown-item" href="sports.php">खेल सुविधा</a>
                    <!-- <a class="dropdown-item" href="transport.php">Transport Facility</a> -->
                    <!-- <a class="dropdown-item" href="otherfacility.php">Extra Curricular Activies</a> -->

                </div>
                <li class="nav-item active">
                <a class="nav-link" href="teacherstaff.php">शिक्षक </a>
            </li>

            </li>
            <li class="nav-item">
                <a class="nav-link" href="contact.php">संपर्क करें</a>
            </li>
        </ul>
    </div>
</nav>